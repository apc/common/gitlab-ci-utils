.node-basic-install:
  image: node:lts-slim
  stage: test
  script:
    - apt-get update
    - apt-get install git curl --yes
    - npm install --unsafe-perm

.node-test:
  image: node:lts-slim
  stage: test
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'
  script:
    - apt-get update
    - apt-get install git curl ${DEPENDENCIES} --yes
    - export PATH="./node_modules/.bin:$PATH"
    # unsafe perm required, for some reason won't run install script otherwise
    - npm install --unsafe-perm

    - run-s test:ci lint:ci:builder
    - run-s lint:ci:report

    - rm -rf public && mkdir public
    - mv lint_badge.svg lint_result.html public
    - mv coverage/lcov-report public/coverage
  artifacts:
    paths:
      - public

.node-typecheck:
  image: node:lts-slim
  stage: test
  script:
    - apt-get update
    - apt-get install git curl ${DEPENDENCIES} --yes
    - export PATH="./node_modules/.bin:$PATH"
    # unsafe perm required, for some reason won't run install script otherwise
    - npm install --unsafe-perm

    - rm -rf public && mkdir -p public
      
      # Typescript check
    - npm install --no-save typescript typescript-coverage-report git+https://gitlab.cern.ch/apc/common/tools/mkbadge.js.git
    - typescript-coverage-report -o public/tc -t 80 | tee report.log
    - mkbadge -l tc-coverage -v $(grep percent report.log|sed -E 's/.*percent[^(]*\(([0-9]*([.][0-9]*)?)%\).*/\1/') --level 50:red,70:orange,80:yellow,90:brightgreen > public/tc_coverage.svg

    - npm run tc:ci:report
    - mv tc_result.html tc_badge.svg public
  artifacts:
    paths:
      - public

.front-build-prod:
  image: node:lts-slim
  stage: build
  script:
    - apt-get update
    - apt-get install git python build-essential ${DEPENDENCIES} --yes
    - export PATH="./node_modules/.bin:$PATH"
    - npm install && npm run install

    - run-s build:prod
  artifacts:
    paths:
      - package-lock.json
      - dist

.front-test:
  image: node:slim
  extends: .node-test
  coverage: '/^Statements\s*:\s*([^%]+)/'
  script:
    - apt-get update
    - apt-get install git python build-essential curl ${DEPENDENCIES} --yes

    - export PATH="./node_modules/.bin:$PATH"
    # unsafe perm required, for some reason won't run install script otherwise
    - npm install --unsafe-perm

    - run-s build test:ci lint:ci:builder
    - run-s lint:ci:report

    - rm -rf public && mkdir public
    - mv lint_badge.svg lint_result.html public
    - mv coverage/lcov-report public/coverage

.webapp-test:
  image: node:slim
  stage: test
  coverage: '/Total:[|]\s*([^%]+)%/'
  script:
    - apt-get update
    - apt-get install git python build-essential lcov ${DEPENDENCIES} --yes

    - export PATH="./node_modules/.bin:$PATH"
    # unsafe perm required, for some reason won't run install script otherwise
    - npm install --unsafe-perm

    - run-s build test:ci

    # Lint
    - apt-get update
    - apt-get install curl --yes
    - run-s lint:ci:report

    - rm -rf public && mkdir public
    - mv lint_badge.svg lint_result.html public

    # Ensure coverage info is absolute in www (nyc>=15.0.0 has relative paths)
    - sed -i "s#SF:src/#SF:www/src/#" www/coverage/lcov.info
    - lcov -a coverage/lcov.info -a www/coverage/lcov.info -o lcov.info
    - lcov --list lcov.info
    - genhtml lcov.info -o public/coverage
  artifacts:
    paths:
      - public

.webapp-build-prod:
  extends: .front-build-prod
  artifacts:
    paths:
      - www/package-lock.json
      - www/dist
      - package-lock.json

.node-flow-coverage:
  stage: test
  # Use a debian (glibc) based image for flow-bin
  image: node:slim
  script:
    - apt-get update
    - apt-get install git ${DEPENDENCIES} --yes
    - export PATH="./node_modules/.bin:$PATH"
    - npm install

    - rm -rf public && mkdir -p public/flow

    # Flow
    - npm install --no-save flow-bin flow-coverage-report
    - flow status
    - flow-coverage-report -i 'src/**/*.js' -o public/flow -t text -t html -t badge
  artifacts:
    paths:
      - public
